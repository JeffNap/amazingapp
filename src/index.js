const express = require('express');

const passport = require('passport');

const mongoose = require('mongoose');

const bodyParser = require('body-parser')

const authRouter = require('../routes');

const cors = require('cors');

const config = require('./config');

const app = express();

mongoose.connect('mongodb+srv://admin:flora040816@sandbox-bh4ww.mongodb.net/amazing_app?retryWrites=true', { useNewUrlParser: true})

require('../auth/index');

app.use(bodyParser.json());

app.use(cors());

app.get('/helloworld', function(req, res) {
	res.status(200).send('Charlemagne The Master');
})

app.post('/helloworld', function(req, res) {
	res.status(200).send(req.body);
})

app.use('/auth', authRouter);

app.use((err, req, res, next) => {
	res.status(422).send({error: err.message})
})

app.get('/userprofile', passport.authenticate('jwt', {session:false}), function(req, res, next) {
	res.status(200).send('user profile');
})

app.get('/', (req, res) => {
	res.send('Hello Jep Nap from Tsupababe!');
})

app.listen(config.port, () => {
	console.log(`Listening at http://localhost:${config.port}`);
});