const bcrypt = require('bcrypt')

const mongoose = require('mongoose')

const Schema = mongoose.Schema

const UserSchema = new Schema({
	email: {
		type: String,
		required: [true, "Email field is required."],
		unique: true
	},
	password: {
		type: String,
		required: [true, "Incorrect password"]
	}


});


// Pre-hook: get plain text password, hast it, and store it

UserSchema.pre('save', async function(next) {
	const user = this;
	
	const hash = await bcrypt.hash(this.password, 10);

	this.password = hash;

	next();

});

// Check user logging in if credentials are correct

UserSchema.methods.isValidPassword = async function(password) {
	const user = this;

	const compare = await bcrypt.compare(password, user.password);

	return compare;
}

const User = mongoose.model('user', UserSchema);

module.exports = User